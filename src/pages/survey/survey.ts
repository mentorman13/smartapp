import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FimPage } from '../fim/fim';

@IonicPage()
@Component({
  selector: 'page-survey',
  templateUrl: 'survey.html',
})
export class SurveyPage {
  parkId:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.parkId = navParams.get('item');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SurveyPage');
  }

  sendSurvey(value) {

    var data = JSON.stringify({
      "user": "1",
      "park": this.parkId,
      "geral": value
    });
    
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    
    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        console.log(this.responseText);
      }
    });
    
    xhr.open("POST", "http://localhost:3000/api/app/users/survey");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("cache-control", "no-cache");
    
    xhr.send(data);
  
    this.navCtrl.setRoot(FimPage);
  }

}
