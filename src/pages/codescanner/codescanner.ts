import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { TimerPage } from '../timer/timer';
declare const Buffer;


@IonicPage()

@Component({
  selector: 'page-codescanner',
  templateUrl: 'codescanner.html',
})

export class CodescannerPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcodeScanner: BarcodeScanner) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CodescannerPage');
  }

  scan() {
    this.barcodeScanner.scan().then(data => {
      var b64string = data.text;
      var buf = Buffer.from(b64string, 'base64');
      var park = JSON.parse(buf.toString());
      this.navCtrl.setRoot(TimerPage,{item:park.park.toString()})
    });      
  }

}
