import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FimPage } from './fim';

@NgModule({
  declarations: [
    FimPage,
  ],
  imports: [
    IonicPageModule.forChild(FimPage),
  ],
})
export class FimPageModule {}
