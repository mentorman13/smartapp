import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import L from 'leaflet';
import { Http } from '@angular/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { TimerPage } from '../timer/timer';
declare const Buffer;

var markerParques = L.featureGroup();

var Indoor = L.icon({
  iconUrl: './assets/imgs/parkinggarage.png',
  iconSize: [32, 37],
  iconAnchor: [16, 37],
  popupAnchor: [-3, -38]
});

var Outdoor = L.icon({
  iconUrl: './assets/imgs/parking.png',
  iconSize: [32, 37],
  iconAnchor: [16, 37],
  popupAnchor: [-3, -38]
});

var marker: any;
var route: any;

function calculaDistancia(latPark, lngPark, latMarker, lngMarker) {
  var request = new XMLHttpRequest();

  request.open('GET', 'https://api.openrouteservice.org/v2/directions/driving-car?api_key=5b3ce3597851110001cf62489eb36e10c25a4d9fbfff6ca76c76c7ca&start=' + lngMarker + ',' + latMarker + '&end=' + lngPark + ',' + latPark);

  request.onreadystatechange = function () {
    if (request.readyState === 4) {
      var resposta = JSON.parse(request.responseText);
      var distancia = resposta.features[0].properties.summary.distance.toFixed(0) + " m";
      return distancia;
    }
  };

  request.send();
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  @ViewChild('map') mapContainer: ElementRef;
  map: any;
  constructor(public navCtrl: NavController, public http: Http, private barcodeScanner: BarcodeScanner) {

  }

  scan() {
    this.barcodeScanner.scan().then(data => {
      var b64string = data.text;
      var buf = Buffer.from(b64string, 'base64');
      var park = JSON.parse(buf.toString());
      this.navCtrl.setRoot(TimerPage, { item: park.park.toString() })
    });
  }

  ionViewDidEnter() {
    this.loadmap();
  }

  loadmap() {
    markerParques = L.featureGroup();
    this.map = L.map("map").fitWorld();
    route = L.geoJSON().addTo(this.map);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      maxZoom: 18
    }).addTo(this.map);
    this.map.locate({
      setView: true,
      maxZoom: 15
    }).on('locationfound', (e) => {
      marker = L.marker([e.latitude, e.longitude], { draggable: 'true' }).on('dragend', () => { this.getParques(marker.getLatLng().lng + "," + marker.getLatLng().lat); route.clearLayers(); })
      this.map.addLayer(marker);

      var data = JSON.stringify({
        "user_id": "1",
        "latitude": e.latitude,
        "longitude": e.longitude
      });

      var xhr = new XMLHttpRequest();
      xhr.withCredentials = true;

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
          console.log(this.responseText);
        }
      });

      xhr.open("POST", "http://localhost:3000/api/app/users/login");
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.setRequestHeader("cache-control", "no-cache");

      xhr.send(data);

    }).on('locationerror', (err) => {
      alert(err.message);
    })
  }

  getParques(coord) {
    markerParques.clearLayers();

    var request = new XMLHttpRequest();

    request.open('GET', 'http://localhost:3000/api/app/parks/' + coord);

    request.onreadystatechange = function () {
      if (this.readyState === 4) {
        var parques = JSON.parse(this.responseText);
        for (var i = 0; i < parques.length; i++) {
          var latparque = parques[i].latitude;
          var lngparque = parques[i].longitude;
          let markerPark = [];
          var rota = [];
          var type;

          if (parques[i].type == "Indoor") {
            type = Indoor;
          }
          else {
            type = Outdoor;
          }

          markerPark[i] = L.marker([latparque, lngparque], { icon: type }).bindPopup("<p>" + parques[i].lot + "</p><p>Distância: " + calculaDistancia(parques[i].latitude, parques[i].longitude, marker.getLatLng().lat, marker.getLatLng().lng) + "<br>Lugares livres: " + parques[i].available + "<br>Avaliação: " + parques[i].avg_aval + "/3").on('click', () => {
            route.clearLayers();
            var request = new XMLHttpRequest();

            request.open('GET', 'https://api.openrouteservice.org/v2/directions/driving-car?api_key=5b3ce3597851110001cf62489eb36e10c25a4d9fbfff6ca76c76c7ca&start=' + marker.getLatLng().lng + ',' + marker.getLatLng().lat + '&end=' + lngparque + ',' + latparque);

            request.onreadystatechange = function () {
              if (this.readyState === 4) {

                var resposta = JSON.parse(this.responseText);

                rota[i] = resposta.features[0].geometry;
                route.addData(rota[i]);
              }
            };
            request.send();
          });

          markerParques.addLayer(markerPark[i]);
        }
      }
    };
    request.send();
    this.map.addLayer(markerParques);
  }
}